import React, {Component} from 'react';

class Login extends Component{
    constructor(props){
        super(props);
        console.log(this.props.title);
    }
    //life cicles
    componentWillMount(){
        console.log("componentWillMount: Se ejecuta antes de montarse");
    }
    componentDidMount(){
        console.log("componentDidMount: se ejecuta ya montado");
    }
    componentWillReceiveProps(nextProps){
        console.log("componentWillReceiveProps: se ejecuta con una actualización de propiedades y recibe como parametros la propiedad modificada", nextProps);
    }
    shouldComponentUpdate(nextProps, nextState){
        console.log("shouldComponentUpdate: Si esta seteado a false corta la renderizacion, en true prosigue", nextProps, nextState);
        return true
    }
    componentWillUpdate(nextProps,nextState){
        console.log("componentWillUpdate: recibe las propiedades futuras", nextProps, nextState);
    }
    componentDidUpdate(nextProps,nextState){
        console.log("componentDidUpdate: recibe las propiedades previas a la actualización", nextProps, nextState);        
    }
    render(){
        return(
            <div>
                {this.props.title}
                <button onClick={this.props.modificar}>Cambiar titulo App </button>
            </div>
        )
    }
}

export default Login;