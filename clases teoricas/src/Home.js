import React, {Component} from 'react';
import Perfil from './Perfil'

class Home extends Component{
    constructor(){
        super()
        this.state = {
            perfiles:[]
        }
    }
    componentDidMount(){
        fetch("https://jsonplaceholder.typicode.com/users")
        .then(res=>res.json())
        .then(
            (result)=>{
                console.log(result);
                this.setState({
                    perfiles:result
                })
            },
            (error) => {
                console.log("error");
            }
        )        
    }
    render(){
        return(
            <div>
                {this.state.perfiles.map(data=><Perfil datos={data}/>)}
               
            </div>
        )
    }
}

export default Home