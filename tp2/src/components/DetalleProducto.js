import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import './DetalleProducto.css'
import Boton from './Boton';



function DetalleProducto({ match }) {

  useEffect(() => {
    fetchDetalle();
    console.log(match);
  }, []);

  const [producto, setDetalle] = useState({})

  const fetchDetalle = async () => {
    const fetchDetalle = await fetch(
      `https://my-json-server.typicode.com/Devether15/fakedb/products/${match.params.id}`
    );
    const producto = await fetchDetalle.json();
    setDetalle(producto);
    console.log(producto);
  }


  return (
    <div className="perfil">
      <img className="detailImg" src={producto.imgUrl} alt="" />
      <div className="detailBox">
        <h2 className="detailName">{producto.name}</h2>
        <div className="descContainer">
            <h4 className="detailDesc">Description: {producto.descripcion}</h4>
          <div>
            <h4 className="detailId">SKU: {producto.id}</h4>        
          </div>
        </div>
      </div>
      <div className="priceContainer">
        <h2 className="detailPrice">Price: {producto.price} <a>USD $</a></h2>
        <div className="bttnContainer">
          <Boton className="detailBttn"/>
        </div>
      </div>
    </div>
  )
}

export default DetalleProducto
