import React,{useState, useEffect} from 'react';
import {Link} from 'react-router-dom';
import './Products.css'
import Boton from './Boton';


function Productos() {

 useEffect(() => {
  fetchProductos();
 },[]);

 const [productos, setProductos] = useState([]);

 const fetchProductos = async () => {
  const data = await fetch('https://my-json-server.typicode.com/Devether15/fakedb/products');
  
  
  const productos = await data.json();
  console.log(productos);
  setProductos(productos);
 }

 return (
  <div className="container">
   {productos.map(producto =>(
     <li key={producto.id} className="productos">
        <Link className="link" to={`/home/producto/${producto.id}`}>
            <div className="producto">
                <img className="productImg" src={producto.imgUrl} alt="" />
                <h2>{producto.name}</h2>
                <h4 className="id">SKU: {producto.id}</h4>
                <h4 className="desc">{producto.descripcion}</h4>
                <h2 className="price">{producto.price} <a>USD $</a></h2>
            </div>
        </Link>
        <Boton/>
     </li>
   ))}
  </div>
 )
}

export default Productos
