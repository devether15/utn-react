import React from 'react'
import {Component, Fragment} from 'react'



class Boton extends Component {
 constructor(props) {
     super(props)
 
     this.state = {
             comprar: false
     }
     this.handleClick = this.handleClick.bind(this);
 }
 
 handleClick() {
     this.setState({ comprar: !this.state.comprar});
 
 }
 
 render() {
     const comprar = this.state.comprar ? 'Cancelar': 'Comprar';
     return (
     <Fragment>
         <button type="button" 
                 onClick={this.handleClick}>{comprar}
         </button>
     </Fragment>
     )
 }
 }
 
 export default Boton
 