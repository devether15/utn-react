import React, { useState, useEffect, Component } from 'react'
import { useHistory } from "react-router-dom"
import './login.css'
import firebase from '../config/firebase'

 function Registro () {
  
  const history = useHistory();

  function handleClick(){
      history.push("/landingpage/registro")
  }

  const [form, setForm] = useState({
        nombre:'',
        apellido: '',
        email: '',
        password:'',
        hobby:''
  })


  function handleSubmit(e) {

    let email = form.email;
    let password = form.password;
    firebase.auth.createUserWithEmailAndPassword(email, password)
    .then((data) =>{
        console.log("Usuario creado", data.user.uid)
        firebase.db.collection("usuarios").add({
            nombre: form.nombre,
            apellido: form.apellido,
            hobby: form.hobby,
            email: form.email,    
            userId: data.user.uid
        })
        .then((data)=>{
            console.log(data)
            history.push("/landingpage/login")
        })  
        .catch((err)=>{
           console.log(err)
           alert(err)
       })
   })
   .catch((error) => {
       console.log("Error", error)
       alert(alert)
   })
   e.preventDefault();
}


function handleChange(e){
    const target = e.target;
    const value = target.value;
    const name = target.name;

    setForm({
      ...form,
      [name]:value
    });

    e.preventDefault();
}
 
   return (
     <form className="base-container" onSubmit={handleSubmit} >
       <div className="content">
         <div className="image">
          REGISTER
         </div>
         <div className="form">
           <div className="form-group">
             <label htmlFor="name">Nombre</label>
             <input type="text" name="nombre" placeholder="Nombre" value={form.nombre} 
                 onChange={handleChange}/>
           </div>
           <div className="form-group">
             <label htmlFor="last-name">Apellido</label>
             <input type="text" name="apellido" placeholder="Apellido" value={form.apellido} 
                 onChange={handleChange}/>
           </div>
           <div className="form-group">
             <label htmlFor="email">Email</label>
             <input type="email" name="email" placeholder="email" value={form.email} 
                 onChange={handleChange} />
           </div>
           <div className="form-group">
             <label htmlFor="hobby">Hobby</label>
             <input type="text" name="hobby" placeholder="hobby" value={form.hobby} 
                 onChange={handleChange} />
           </div>
           <div className="form-group">
             <label htmlFor="password">Contraseña</label>
             <input type="password" name="password" placeholder="password" value={form.password} 
                 onChange={handleChange} />
           </div>
         </div>
       </div>
       <div className="footer">
         <button type="submit" className="btn" onClick={handleClick}>
           Registrarse
         </button>
       </div>
     </form>
   );

 }

export default Registro