import React from 'react'
import { Switch, Route} from 'react-router-dom'
import { Fragment } from 'react'
import Nav from './Nav'
import Productos from './Productos'
import DetalleProducto from  './DetalleProducto'
import Login from './Login'

function Home() {
 return (
  <Fragment>
  <Nav/>
  <Switch>
  <Route path="/home/productos" component={Productos}/>
  <Route path="/home/producto/:id" component={DetalleProducto}/>
  <Route path="/landingpage/login" component={Login}/>
  </Switch>
  </Fragment>
 )
}

export default Home
