import * as firebase from 'firebase'

 
  var firebaseConfig = {
    apiKey: "AIzaSyC_jAjZRjmnEy2UWCXczGSb_w8KNP9Pn4M",
    authDomain: "utn-react-2a044.firebaseapp.com",
    databaseURL: "https://utn-react-2a044.firebaseio.com",
    projectId: "utn-react-2a044",
    storageBucket: "utn-react-2a044.appspot.com",
    messagingSenderId: "20329305601",
    appId: "1:20329305601:web:a09309a5725b4ac8d1d200"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

const db = firebase.firestore();
db.settings({timestampsInSnapshots: true});
firebase.db = db;

firebase.auth = firebase.auth();

export default firebase;